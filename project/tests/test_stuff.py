from project.code import some_function


def test_some_function():
    assert some_function()


def test_some_function_is_true():
    assert some_function() is True


def test_some_function_is_not_false():
    assert some_function() is not False


def test_some_function_return_is_bool_type():
    assert type(some_function()) is bool
