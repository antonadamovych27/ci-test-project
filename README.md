# ci-test-project



## Getting started
- create virtual environment
- install requirements: `pip install -r requirements.txt`
- run tests: `pytest --cov`

## Additional documentation

- https://docs.gitlab.com/ee/ci/yaml/#parallel
- https://coverage.readthedocs.io/en/latest/cmd.html#cmd-combine
- https://coverage.readthedocs.io/en/latest/cmd.html#cmd-report
- https://github.com/mark-adams/pytest-test-groups
- https://pytest-xdist.readthedocs.io/en/latest/distribution.html


## Notes
- "--randomly-dont-reorganize" is used to avoid reordering of tests if pytest-randomly is installed
- coverage combine is used to combine coverage data from parallel jobs, need to override coverage output file